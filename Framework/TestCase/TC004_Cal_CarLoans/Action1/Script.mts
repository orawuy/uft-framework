﻿iRowCount = Datatable.getSheet("TC003 [TC003_CAL_Conditinal]").getRowCount
'----------------------------- Data ------------------------------'
TabName = Trim((DataTable("TabName","TC004 [TC004_Cal_CarLoans]")))
SelectType = Trim((DataTable("SelectType","TC004 [TC004_Cal_CarLoans]")))
CarPriceNoVAT = Trim((DataTable("CarPriceNoVAT","TC004 [TC004_Cal_CarLoans]")))
DownPercent = Trim((DataTable("DownPercent","TC004 [TC004_Cal_CarLoans]")))
InstallmentPeriod = Trim((DataTable("InstallmentPeriod","TC004 [TC004_Cal_CarLoans]")))
RatePerYear = Trim((DataTable("RatePerYear","TC004 [TC004_Cal_CarLoans]")))
ExpectedCarPriceNoVAT = Trim((DataTable("ExpectedCarPriceNoVAT","TC004 [TC004_Cal_CarLoans]")))
ExpectedDownPercent = Trim((DataTable("ExpectedDownPercent","TC004 [TC004_Cal_CarLoans]")))
ExpectedInstallmentPeriod = Trim((DataTable("ExpectedInstallmentPeriod","TC004 [TC004_Cal_CarLoans]")))
ExpectedRatePerYear = Trim((DataTable("ExpectedRatePerYear","TC004 [TC004_Cal_CarLoans]")))
ExpectedHirePurchase = Trim((DataTable("ExpectedHirePurchase","TC004 [TC004_Cal_CarLoans]")))
ExpectedMonthlyInstallment = Trim((DataTable("ExpectedMonthlyInstallment","TC004 [TC004_Cal_CarLoans]")))
'----------------------------- Data ------------------------------'
			 

Call FW_OpenWebBrowser("เปิดเว็บ","https://www.krungsri.com/bank/th/Other/Calculator/personal-revolving-calculator/loan-repayment-calculator.html","IE")
Call FW_Link("เลือกแถบคำนวณสินเชื่อรถยนต์","คำนวณสินเชื่อรถยนต์ l","คำนวณสินเชื่อรถยนต์ l","คำนวณสินเชื่อรถยนต์")  
Call FW_WebList("เลือกประเภทสินเชื่อ","คำนวณสินเชื่อรถยนต์ l","คำนวณสินเชื่อรถยนต์ l","ประเภทสินเชื่อ",SelectType)
Call FW_WebEditAndVerifyResult("กรอกราคารถ(ไม่รวม VAT)","คำนวณสินเชื่อรถยนต์ l","คำนวณสินเชื่อรถยนต์ l","ราคารถ (ไม่รวม VAT)",CarPriceNoVAT,ExpectedCarPriceNoVAT)
Call FW_WebEditAndVerifyResult("กรอกเงินดาวน์%","คำนวณสินเชื่อรถยนต์ l","คำนวณสินเชื่อรถยนต์ l","เงินดาวน์%",DownPercent,ExpectedDownPercent)
Call FW_WebListAndVerifyResult("เลือกระยะเวลาผ่อนชำระ","คำนวณสินเชื่อรถยนต์ l","คำนวณสินเชื่อรถยนต์ l","ระยะเวลาผ่อนชำระ",InstallmentPeriod,ExpectedInstallmentPeriod)
Call FW_WebEditAndVerifyResult("กรอกอัตราดอกเบี้ยต่อปี","คำนวณสินเชื่อรถยนต์ l","คำนวณสินเชื่อรถยนต์ l","อัตราดอกเบี้ยต่อปี",RatePerYear,ExpectedRatePerYear)
Call FW_WebButtonAndVerifyEditResult("กดปุ่มเริ่มคำนวณ","คำนวณสินเชื่อรถยนต์ l","คำนวณสินเชื่อรถยนต์ l","เริ่มคำนวณ","ยอดจัดเช่าซื้อ",ExpectedHirePurchase)
Call FW_ValidateEditMessage("ตรวจสอบยอดจัดเช่าซื้อ","คำนวณสินเชื่อรถยนต์ l","คำนวณสินเชื่อรถยนต์ l","ยอดจัดเช่าซื้อ",ExpectedHirePurchase)
Call FW_ValidateEditMessage("ตรวจสอบค่างวดต่อเดือน (รวม VAT)","คำนวณสินเชื่อรถยนต์ l","คำนวณสินเชื่อรถยนต์ l","ค่างวดต่อเดือน (รวม VAT)",ExpectedMonthlyInstallment)
Call FW_CloseWebBrowserIE("ปิดเว็บ")


