﻿iRowCount = Datatable.getSheet("TC005 [TC005_Cal_KrungsriExclusive]").getRowCount
'----------------------------- Data ------------------------------'
BeginAtAge = Trim((DataTable("BeginAtAge","TC005 [TC005_Cal_KrungsriExclusive]")))
InitialAmount = Trim((DataTable("InitialAmount","TC005 [TC005_Cal_KrungsriExclusive]")))
EveryYear = Trim((DataTable("EveryYear","TC005 [TC005_Cal_KrungsriExclusive]")))
EveryYearPercent = Trim((DataTable("EveryYearPercent","TC005 [TC005_Cal_KrungsriExclusive]")))
Duration = Trim((DataTable("Duration","TC005 [TC005_Cal_KrungsriExclusive]")))
RateOfReturn = Trim((DataTable("RateOfReturn","TC005 [TC005_Cal_KrungsriExclusive]")))
Inflation = Trim((DataTable("Inflation","TC005 [TC005_Cal_KrungsriExclusive]")))
MoneyWanted = Trim((DataTable("MoneyWanted","TC005 [TC005_Cal_KrungsriExclusive]")))
FirstInvestment = Trim((DataTable("FirstInvestment","TC005 [TC005_Cal_KrungsriExclusive]")))
ExpectedBeginAtAge = Trim((DataTable("ExpectedBeginAtAge","TC005 [TC005_Cal_KrungsriExclusive]")))
ExpectedInitialAmount = Trim((DataTable("ExpectedInitialAmount","TC005 [TC005_Cal_KrungsriExclusive]")))
ExpectedEveryYear = Trim((DataTable("ExpectedEveryYear","TC005 [TC005_Cal_KrungsriExclusive]")))
ExpectedEveryYearPercent = Trim((DataTable("ExpectedEveryYearPercent","TC005 [TC005_Cal_KrungsriExclusive]")))
ExpectedDuration = Trim((DataTable("ExpectedDuration","TC005 [TC005_Cal_KrungsriExclusive]")))
ExpectedRateOfReturn = Trim((DataTable("ExpectedRateOfReturn","TC005 [TC005_Cal_KrungsriExclusive]")))
ExpectedInflation = Trim((DataTable("ExpectedInflation","TC005 [TC005_Cal_KrungsriExclusive]")))
ExpectedMoneyWanted = Trim((DataTable("ExpectedMoneyWanted","TC005 [TC005_Cal_KrungsriExclusive]")))
'----------------------------- Data ------------------------------'


Call FW_OpenWebBrowser("เปิดเว็บ","https://www.krungsri.com/bank/th/Other/Calculator/personal-revolving-calculator/loan-repayment-calculator.html","IE")
Call FW_Link("เลือกแถบเครื่องคำนวณ กรุงศรีเอ็กซ์คลูซีฟ","คำนวณการผ่อนชำระสินเชื่อบุคคล","คำนวณการผ่อนชำระสินเชื่อบุคคล","เครื่องคำนวณ กรุงศรีเอ็กซ์คลูซ")
Call FW_WebEditAndVerifyResult("กรอกเริ่มต้นลงทุนที่อายุ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","กรอกเริ่มต้นลงทุนที่อายุ",BeginAtAge,ExpectedBeginAtAge)
Call FW_WebEditAndVerifyResult("กรอกจำนวนเงินลงทุนครั้งแรก","เครื่องคำนวณ เอ็กซ์คลูซีฟ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","กรอกจำนวนเงินลงทุนครั้งแรก",InitialAmount,ExpectedInitialAmount)
Call FW_WebEditAndVerifyResult("กรอกลงทุนเพิ่มทุกปี","เครื่องคำนวณ เอ็กซ์คลูซีฟ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","กรอกลงทุนเพิ่มทุกปี",EveryYear,ExpectedEveryYear)
Call FW_WebEditAndVerifyResult("กรอกทุกๆปีจะลงทุนเพิ่ม","เครื่องคำนวณ เอ็กซ์คลูซีฟ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","กรอกทุกๆปีจะลงทุนเพิ่ม",EveryYearPercent,ExpectedEveryYearPercent)
Call FW_WebEditAndVerifyResult("ระยะเวลาการลงทุน","เครื่องคำนวณ เอ็กซ์คลูซีฟ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","ระยะเวลาการลงทุน",Duration,ExpectedDuration) @@ script infofile_;_ZIP::ssf7.xml_;_
Call FW_WebEditAndVerifyResult("กรอกผลตอบแทนเฉลี่ยต่อปี","เครื่องคำนวณ เอ็กซ์คลูซีฟ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","กรอกผลตอบแทนเฉลี่ยต่อปี",RateOfReturn,ExpectedRateOfReturn)
Call FW_WebEditAndVerifyResult("กรอกอัตราเงินเฟ้อ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","กรอกอัตราเงินเฟ้อ",Inflation,ExpectedInflation) @@ script infofile_;_ZIP::ssf9.xml_;_
Call FW_WebEditAndVerifyResult("กรอกจำนวนเงินที่นักลงทุนต้องการมี","เครื่องคำนวณ เอ็กซ์คลูซีฟ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","กรอกจำนวนเงินที่นักลงทุนต้องการมี",MoneyWanted,ExpectedMoneyWanted) 
Call FW_WebButton("กดปุ่มเริ่มคำนวณ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","เริ่มคำนวณ")  @@ script infofile_;_ZIP::ssf11.xml_;_
Call FW_ValidateEditMessage("ตรวจสอบจำนวนเงินลงทุนครั้งแรก","เครื่องคำนวณ เอ็กซ์คลูซีฟ","เครื่องคำนวณ เอ็กซ์คลูซีฟ","ตรวจสอบจำนวนเงินลงทุนครั้งแรก",FirstInvestment)  @@ script infofile_;_ZIP::ssf12.xml_;_
Call FW_CloseWebBrowserIE("ปิดเว็บ")
